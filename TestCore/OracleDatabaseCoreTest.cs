﻿using NUnit.Framework;
using Oracle.ManagedDataAccess.Client;
using Mediterrum.OracleTools;
using System;
using System.Collections.Generic;
using System.Data;

namespace Test
{
	public class OracleDatabaseCoreTest
	{
		[SetUp]
		public void CreateTestPlatform()
		{
			using (OracleDatabase database = new OracleDatabase())
			{
				if (
					database.NonQuery(@"
						create table UNIT_TEST_TMP_TABLE (
							A_INTEGER   number,
							A_DECIMAL   number(12, 5),
							A_STRING    varchar2(100),
							A_DATE      date,
							A_BOOLEAN   number(1, 0),
							A_CLOB      clob,
							A_BLOB      blob
						)
					")
					== OracleDatabase.NonQueryFail
					&& database.LastErrorCode != "ORA-00955"
				)
				{
					Assert.Fail("Create test table: " + database.LastException.Message);
				}
			} // using (OracleDatabase database)
		} // CreateTestPlatform()

		[TearDown]
		public void DropTestPlatform()
		{
			using (OracleDatabase database = new OracleDatabase())
			{
				database.NonQuery("drop table UNIT_TEST_TMP_TABLE");
				database.NonQuery("drop procedure UNIT_TEST_TMP_INSERT_RETURN");
			} // using (OracleDatabase database)
		} // DropTestPlatform()

		[Test]
		public void TestDateTime()
		{
			using (OracleDatabase database = new OracleDatabase())
			{
				OracleDatabase.ForceLocalDate = true;

				OracleCommand command = new OracleCommand(@"
					insert into UNIT_TEST_TMP_TABLE
					( A_STRING, A_DATE )
					values
					( 'date', :I_DATE )
				");

				command.Parameters.Add("I_DATE", new DateTime(2000, 1, 1, 12, 0, 0, DateTimeKind.Unspecified));

				if (database.NonQuery(command) == OracleDatabase.NonQueryFail)
				{
					Assert.Fail("Insert date (1): " + database.LastException.Message);
				}

				command.Parameters["I_DATE"].Value = new DateTime(2000, 1, 1, 12, 0, 0, DateTimeKind.Local);

				if (database.NonQuery(command) == OracleDatabase.NonQueryFail)
				{
					Assert.Fail("Insert date (2): " + database.LastException.Message);
				}


				command = new OracleCommand("select * from UNIT_TEST_TMP_TABLE where A_STRING = 'date'");

				DataTable data = database.QueryTable(command);

				if (data == null)
				{
					Assert.Fail("Select: " + database.LastException.Message);
				}

				if (((DateTime)data.Rows[0]["A_DATE"]).CompareTo((DateTime)data.Rows[1]["A_DATE"]) != 0)
				{
					Assert.Fail("DateTimes");
				}
			} // using (OracleDatabase database)
		} // TestTime()

		[Test]
		public void TestProcedure()
		{
			using (OracleDatabase database = new OracleDatabase())
			{
				if (
					database.NonQuery(@"
						create or replace procedure UNIT_TEST_TMP_INSERT_RETURN (
							IO_A_INTEGER  in out UNIT_TEST_TMP_TABLE.A_INTEGER%type,
							IO_A_DECIMAL  in out UNIT_TEST_TMP_TABLE.A_DECIMAL%type,
							IO_A_STRING   in out UNIT_TEST_TMP_TABLE.A_STRING%type,
							IO_A_DATE     in out UNIT_TEST_TMP_TABLE.A_DATE%type,
							IO_A_BOOLEAN  in out UNIT_TEST_TMP_TABLE.A_BOOLEAN%type,
							O_OUTPUT         out sys_refcursor
						)
						is
						begin

							delete from UNIT_TEST_TMP_TABLE;

							insert into UNIT_TEST_TMP_TABLE (
								A_INTEGER,
								A_DECIMAL,
								A_STRING,
								A_DATE,
								A_BOOLEAN
							)
							values (
								IO_A_INTEGER,
								IO_A_DECIMAL,
								IO_A_STRING,
								IO_A_DATE,
								IO_A_BOOLEAN
							);

							open O_OUTPUT for
							select
								*
							from UNIT_TEST_TMP_TABLE;

						end UNIT_TEST_TMP_INSERT_RETURN;
					") == OracleDatabase.NonQueryFail
				)
				{
					Assert.Fail("Create: " + database.LastException.Message);
				}

				OracleCommand command = database.NewSPCommand("UNIT_TEST_TMP_INSERT_RETURN");

				command.Parameters.Add("IO_A_INTEGER", OracleDbType.Int32, 1, ParameterDirection.InputOutput);
				command.Parameters.Add("IO_A_DECIMAL", OracleDbType.Double, 2.3, ParameterDirection.InputOutput);
				command.Parameters.Add("IO_A_STRING", OracleDbType.Varchar2, "procedure", ParameterDirection.InputOutput);
				command.Parameters.Add("IO_A_DATE", OracleDbType.Date, new DateTime(2000, 1, 1, 12, 0, 0, DateTimeKind.Unspecified), ParameterDirection.InputOutput);
				command.Parameters.Add("IO_A_BOOLEAN", OracleDbType.Boolean, true, ParameterDirection.InputOutput);
				command.Parameters.Add("O_OUTPUT", OracleDbType.RefCursor, ParameterDirection.Output);

				DataSet data = database.QuerySet(command);

				if (data == null)
				{
					Assert.Fail("Procedure: " + database.LastException.Message);
				}

				if (data.Tables[0].Rows.Count == 0)
				{
					Assert.Fail("Output table");
				}
				if ((int)data.Tables[0].Rows[0]["A_INTEGER"] != 1)
				{
					Assert.Fail("Integer");
				}
				if ((double)data.Tables[0].Rows[0]["A_DECIMAL"] != 2.3)
				{
					Assert.Fail("Decimal");
				}
				if ((string)data.Tables[0].Rows[0]["A_STRING"] != "procedure")
				{
					Assert.Fail("String");
				}
				if (new DateTime(2000, 1, 1, 12, 0, 0, DateTimeKind.Local).CompareTo((DateTime)data.Tables[0].Rows[0]["A_DATE"]) != 0)
				{
					Assert.Fail("Date");
				}
				if (Convert.ToBoolean(data.Tables[0].Rows[0]["A_BOOLEAN"]) != true)
				{
					Assert.Fail("Boolean");
				}
			} // using (OracleDatabase database)
		} // TestProcedure()

		[Test]
		public void TestHelpers()
		{
			using (OracleDatabase database = new OracleDatabase())
			{
				if (
					database.NonQuery(@"
						insert into UNIT_TEST_TMP_TABLE
						( A_STRING, A_INTEGER, A_DECIMAL, A_DATE, A_BOOLEAN )
						select 'list', 1, 2.3, to_date('2000-01-01', 'yyyy-mm-dd'), 1 from DUAL
						union 
						select 'list', 2, null, to_date('2000-01-01', 'yyyy-mm-dd'), 0 from DUAL
						union 
						select 'list', 3, 1.2, null, null from DUAL
					")
					== OracleDatabase.NonQueryFail
				)
				{
					Assert.Fail("Insert: " + database.LastException.Message);
				}

				DataTable data = database.QueryTable(@"
					select
						A_INTEGER,
						A_DECIMAL,
						A_STRING,
						A_DATE,
						A_BOOLEAN
					from UNIT_TEST_TMP_TABLE
					where
						A_STRING = 'list'
				");

				if (data == null)
				{
					Assert.Fail("Select: " + database.LastException.Message);
				}

				List<TestDataClass> dataList = OracleDatabase.ToList<TestDataClass>(data);

				if (dataList.Count != 3)
				{
					Assert.Fail("Class list");
				}
				if (dataList[0].AInteger != 1)
				{
					Assert.Fail("Class list integer");
				}
				if (dataList[0].ADecimal != 2.3)
				{
					Assert.Fail("Class list decimal");
				}
				if (dataList[0].AString != "list")
				{
					Assert.Fail("Class list string");
				}
				if (new DateTime(2000, 1, 1).CompareTo(dataList[0].ADate) != 0)
				{
					Assert.Fail("Class list date");
				}
				if (dataList[0].ABoolean != true)
				{
					Assert.Fail("Class list boolean");
				}

				List<int> intList = OracleDatabase.ToList<int>(data, "A_INTEGER");

				if (intList.Count != 3 || intList[0] != 1 || intList[1] != 2 || intList[2] != 3)
				{
					Assert.Fail("Int list");
				}

				List<double?> doubleList = OracleDatabase.ToList<double?>(data, "A_DECIMAL");

				if (doubleList.Count != 3 || doubleList[0] != 2.3 || doubleList[1] != null || doubleList[2] != 1.2)
				{
					Assert.Fail("Double list");
				}

				List<DateTime?> dateList = OracleDatabase.ToList<DateTime?>(data, "A_DATE");

				if (dateList.Count != 3 || new DateTime(2000, 1, 1).CompareTo(dateList[0]) != 0)
				{
					Assert.Fail("Date list");
				}

				List<bool?> boolList = OracleDatabase.ToList<bool?>(data, "A_BOOLEAN");

				if (boolList.Count != 3 || boolList[0] != true || boolList[1] != false || boolList[2] != null)
				{
					Assert.Fail("Date list");
				}

				TestToList<byte>(data, "A_INTEGER");
				TestToList<short>(data, "A_INTEGER");
				TestToList<long>(data, "A_INTEGER");
				TestToList<float>(data, "A_DECIMAL");
				TestToList<decimal>(data, "A_DECIMAL");
				TestToList<string>(data, "A_STRING");
				TestToList<DateTime>(data, "A_DATE");
				TestToList<bool>(data, "A_BOOLEAN");

				TestToList<byte>(data, 0);
				TestToList<short>(data, 0);
				TestToList<int>(data, 0);
				TestToList<long>(data, 0);
				TestToList<float>(data, 1);
				TestToList<decimal>(data, 1);
				TestToList<string>(data, 2);
				TestToList<DateTime>(data, 3);
				TestToList<bool>(data, 4);

				try
				{
					OracleDatabase.ToList<int, double>(data, "A_INTEGER", "A_DECIMAL");
				}
				catch (Exception exception)
				{
					Assert.Fail("ToList<int, double>(column): " + exception.Message);
				}
				try
				{
					OracleDatabase.ToList<int, double>(data, 0, 1);
				}
				catch (Exception exception)
				{
					Assert.Fail("ToList<int, double>(int): " + exception.Message);
				}
				try
				{
					OracleDatabase.ToDictionary<int, TestDataClass>(data, "A_INTEGER");
				}
				catch (Exception exception)
				{
					Assert.Fail("ToDictionary<int, TestDataClass>(column): " + exception.Message);
				}
				try
				{
					OracleDatabase.ToDictionary<int, TestDataClass>(data, 0);
				}
				catch (Exception exception)
				{
					Assert.Fail("ToDictionary<int, TestDataClass>(index): " + exception.Message);
				}
				try
				{
					OracleDatabase.ToDictionary<int, DateTime>(data, "A_INTEGER", "A_DATE");
				}
				catch (Exception exception)
				{
					Assert.Fail("ToDictionary<int, DateTime>(column): " + exception.Message);
				}
				try
				{
					OracleDatabase.ToDictionary<int, DateTime>(data, 0, 3);
				}
				catch (Exception exception)
				{
					Assert.Fail("ToDictionary<int, DateTime>(index): " + exception.Message);
				}
				try
				{
					OracleDatabase.ToDictionary<int, bool>(data, 0, 4);
				}
				catch (Exception exception)
				{
					Assert.Fail("ToDictionary<int, bool>(index): " + exception.Message);
				}
			} // using (OracleDatabase database)
		} // TestHelpers()

		[Test]
		public void TestCLob()
		{
			using (OracleDatabase database = new OracleDatabase())
			{
				if (
					database.NonQuery(@"
						create or replace procedure UNIT_TEST_TMP_CLOB (
							O_A_CLOB  out clob,
							O_OUTPUT  out sys_refcursor
						)
						is
						begin

							select
								A_CLOB
							into
								O_A_CLOB
							from UNIT_TEST_TMP_TABLE
							where
								A_STRING = 'clob';

							open O_OUTPUT for
							select
								A_CLOB
							from UNIT_TEST_TMP_TABLE
							where
								A_STRING = 'clob';

						end UNIT_TEST_TMP_CLOB;
					") == OracleDatabase.NonQueryFail
				)
				{
					Assert.Fail("Create procedure: " + database.LastException.Message);
				}

				if (
					database.NonQuery(@"
						insert into UNIT_TEST_TMP_TABLE
						( A_STRING, A_CLOB )
						values
						( 'clob', 'clob content' )
					")
					== OracleDatabase.NonQueryFail
				)
				{
					Assert.Fail("Insert: " + database.LastException.Message);
				}


				DataRow dataRow = database.QueryRow(@"
					select
						A_STRING,
						A_CLOB
					from UNIT_TEST_TMP_TABLE
					where
						A_STRING = 'clob'
				");

				if (dataRow == null)
				{
					Assert.Fail("Select: " + database.LastException.Message);
				}
				if ((string)dataRow["A_CLOB"] != "clob content")
				{
					Assert.Fail("Select data");
				}


				OracleCommand command = database.NewSPCommand("UNIT_TEST_TMP_CLOB");

				command.Parameters.Add("O_A_CLOB", OracleDbType.Clob, ParameterDirection.Output);
				command.Parameters.Add("O_OUTPUT", OracleDbType.RefCursor, ParameterDirection.Output);

				DataSet dataSet = database.QuerySet(command);

				if (dataSet == null)
				{
					Assert.Fail("Procedure call: " + database.LastException.Message);
				}

				if ((string)dataSet.Tables[0].Rows[0]["A_CLOB"] != "clob content")
				{
					Assert.Fail("Output parameter data");
				}
				if ((string)dataSet.Tables[1].Rows[0]["A_CLOB"] != "clob content")
				{
					Assert.Fail("CursorData");
				}

				database.NonQuery("drop procedure UNIT_TEST_TMP_CLOB");
			}
		} // TestCLob()

		[Test]
		public void TestBlob()
		{
			using OracleDatabase database = new OracleDatabase();

			if (
				database.NonQuery(@"
					create or replace procedure UNIT_TEST_TMP_BLOB (
						O_A_BLOB  out blob,
						O_OUTPUT  out sys_refcursor
					)
					is
					begin

						select
							A_BLOB
						into
							O_A_BLOB
						from UNIT_TEST_TMP_TABLE
						where
							A_STRING = 'blob'
							and ROWNUM = 1;

						open O_OUTPUT for
						select
							A_BLOB
						from UNIT_TEST_TMP_TABLE
						where
							A_STRING = 'blob';

					end UNIT_TEST_TMP_BLOB;
				") == OracleDatabase.NonQueryFail
			)
			{
				Assert.Fail("Create procedure: " + database.LastException.Message);
			}

			OracleCommand command = new OracleCommand(@"
					insert into UNIT_TEST_TMP_TABLE
					( A_STRING, A_BLOB )
					values
					( 'blob', :i_BLOB )
				");
			byte[] blob = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

			command.Parameters.Add("i_BLOB", OracleDbType.Blob, blob.Length, blob, ParameterDirection.Input);

			if (database.NonQuery(command) == OracleDatabase.NonQueryFail)
			{
				Assert.Fail("Insert: " + database.LastException.Message);
			}


			DataRow dataRow = database.QueryRow(@"
				select
					A_STRING,
					A_BLOB
				from UNIT_TEST_TMP_TABLE
				where
					A_STRING = 'blob'
			");

			if (dataRow == null)
			{
				Assert.Fail("Select: " + database.LastException.Message);
			}
			if (dataRow["A_BLOB"].GetType() != typeof(byte[]))
			{
				Assert.Fail("select: invalid data type");
			}
			if (((byte[])dataRow["A_BLOB"]).Length != 10)
			{
				Assert.Fail("select: invalid data length");
			}


			command = database.NewSPCommand("UNIT_TEST_TMP_BLOB");

			command.Parameters.Add("O_A_BLOB", OracleDbType.Blob, ParameterDirection.Output);
			command.Parameters.Add("O_OUTPUT", OracleDbType.RefCursor, ParameterDirection.Output);

			DataSet dataSet = database.QuerySet(command);

			if (dataSet == null)
			{
				Assert.Fail("Procedure call: " + database.LastException.Message);
			}

			if (((byte[])dataSet.Tables[0].Rows[0]["A_BLOB"]).Length != 10)
			{
				Assert.Fail("Output parameter data");
			}
			if (((byte[])dataSet.Tables[1].Rows[0]["A_BLOB"]).Length != 10)
			{
				Assert.Fail("CursorData");
			}

			TestToList<byte[]>(dataSet.Tables[1], "A_BLOB");

			database.NonQuery("drop procedure UNIT_TEST_TMP_BLOB");
		} // TestBlob()

		[Test]
		public void TestNonQuery()
		{
			using (OracleDatabase database = new OracleDatabase())
			{
				int? rowCount =
					database.NonQuery(@"
						insert into UNIT_TEST_TMP_TABLE
						( A_STRING, A_INTEGER, A_DECIMAL, A_DATE )
						values
						( 'error', null, null, null )
					");

				if (rowCount == OracleDatabase.NonQueryFail)
				{
					Assert.Fail("Insert: " + database.LastException.Message);
				}
				if (rowCount != 1)
				{
					Assert.Fail("Insert: " + rowCount);
				}


				if (
					database.NonQuery(@"
						insert into UNIT_TEST_TMP_TABLE
						( A_STRING, A_INTEGER, A_DECIMAL, A_DATE )
						values
					")
					!= OracleDatabase.NonQueryFail
				)
				{
					Assert.Fail("Insert error: " + database.LastErrorCode);
				}
			}
		} // TestNonQuery()

		[Test]
		public void TestBulkSet()
		{
			using (OracleDatabase database = new OracleDatabase())
			{
				List<TestDataClass> list = new List<TestDataClass>()
				{
					new TestDataClass() { AInteger = 1, AString = "bulkset", ADecimal = 1,        ABlob = new byte[] { 1, 2, 3 }, ABoolean = false },
					new TestDataClass() { AInteger = 2, AString = "bulkset", ADate = DateTime.Now },
					new TestDataClass() { AInteger = 3, AString = "bulkset", ADecimal = 3.5,      ABlob = new byte[] { 4, 5 }, ABoolean = true  }
				};


				database.BeginTransaction();

				OracleCommand insertCommand = new OracleCommand(@"
						insert into UNIT_TEST_TMP_TABLE
						( A_STRING, A_INTEGER, A_DECIMAL, A_BLOB, A_BOOLEAN )
						values
						( :i_A_STRING, :i_A_INTEGER, :i_A_DECIMAL, :i_A_BLOB, :i_A_BOOLEAN )
					");

				OracleDatabase.BulkSet(insertCommand, list, new string[] { "A_STRING", "A_INTEGER", "A_DECIMAL", "A_BLOB", "A_BOOLEAN" });

				if (database.NonQuery(insertCommand) == OracleDatabase.NonQueryFail)
				{
					Assert.Fail("List, partial: " + database.LastException.Message);
				}

				DataTable data = database.QueryTable(@"select * from UNIT_TEST_TMP_TABLE where A_STRING = 'bulkset'");

				if (data == null)
				{
					Assert.Fail("Select (List, partial): " + database.LastException.Message);
				}
				if (data.Rows.Count != list.Count)
				{
					Assert.Fail("Select (List, partial): row count");
				}

				database.Rollback();


				database.BeginTransaction();

				insertCommand = new OracleCommand(@"
						insert into UNIT_TEST_TMP_TABLE
						( A_STRING, A_INTEGER, A_DECIMAL, A_BLOB, A_BOOLEAN )
						values
						( :i_A_STRING, :i_A_INTEGER, :i_A_DECIMAL, :i_A_BLOB, :i_A_BOOLEAN )
					");

				OracleDatabase.BulkSet(insertCommand, list);

				if (database.NonQuery(insertCommand) == OracleDatabase.NonQueryFail)
				{
					Assert.Fail("List, full: " + database.LastException.Message);
				}

				data = database.QueryTable(@"select * from UNIT_TEST_TMP_TABLE where A_STRING = 'bulkset'");

				if (data == null)
				{
					Assert.Fail("Select (List, full): " + database.LastException.Message);
				}
				if (data.Rows.Count != list.Count)
				{
					Assert.Fail("Select (List, full): row count");
				}

				database.Rollback();

			} // using (OracleDatabase database = new OracleDatabase())
		} // TestBulkSet()

		[Test]
		public void TestTransaction()
		{
			using (OracleDatabase database = new OracleDatabase())
			{
				database.BeginTransaction();

				if (
					database.NonQuery(@"
						insert into UNIT_TEST_TMP_TABLE
						( A_STRING, A_INTEGER, A_DECIMAL, A_DATE )
						select 'transaction', 1, 2.3, null from DUAL
					")
					== OracleDatabase.NonQueryFail
				)
				{
					Assert.Fail("Insert: " + database.LastException.Message);
				}


				DataTable data = database.QueryTable(@"
					select
						A_INTEGER,
						A_DECIMAL,
						A_STRING,
						A_DATE
					from UNIT_TEST_TMP_TABLE
					where
						A_STRING = 'transaction'
				");

				if (data == null)
				{
					Assert.Fail("Select (in transaction): " + database.LastException.Message);
				}
				if (data.Rows.Count != 1)
				{
					Assert.Fail("Select (in transaction): Row count");
				}


				database.Rollback();


				data = database.QueryTable(@"
					select
						A_INTEGER,
						A_DECIMAL,
						A_STRING,
						A_DATE
					from UNIT_TEST_TMP_TABLE
					where
						A_STRING = 'transaction'
				");

				if (data == null)
				{
					Assert.Fail("Select (after rollback): " + database.LastException.Message);
				}
				if (data.Rows.Count != 0)
				{
					Assert.Fail("Select (after rollback): Row count");
				}
			}
		} // TestTransaction()

		private void TestToList<T>(DataTable data, string column)
		{
			try
			{
				OracleDatabase.ToList<T>(data, column);
			}
			catch (Exception exception)
			{
				Assert.Fail("ToList<" + typeof(T).ToString() + ">(column): " + exception.Message);
			}
		} // ToTestList(string)

		private void TestToList<T>(DataTable data, int index)
		{
			try
			{
				OracleDatabase.ToList<T>(data, index);
			}
			catch (Exception exception)
			{
				Assert.Fail("ToList<" + typeof(T).ToString() + ">(index): " + exception.Message);
			}
		} // ToTestList(string)
	} // class OracleDatabaseTest

	class TestDataClass
	{
		public int? AInteger { get; set; }
		public double? ADecimal { get; set; }
		public string AString { get; set; }
		public DateTime? ADate { get; set; }
		public bool? ABoolean { get; set; }
		public byte[] ABlob { get; set; }
	}
} // namespace Test