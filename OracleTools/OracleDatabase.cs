﻿#if NETCORE
using Microsoft.Extensions.Configuration;
using System.IO;
#elif NETFRAMEWORK
using System.Configuration;
#endif
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using Mediterrum.DatabaseTools;

namespace Mediterrum.OracleTools
{
	public class OracleDatabase : DataTableTool, IDisposable
	{
		#region Properties
		/// <summary>
		/// Delegate for error logger, which is called after every error
		/// </summary>
		public delegate void ErrorLoggerDelegate(Exception exception, string commandText = null, Dictionary<string, string> parameters = null);

		#if NETFRAMEWORK
		/// <summary>
		/// Name of the connection string used by default. By default first in app.config/ConnectionStrings
		/// </summary>
		#elif NETCORE
		/// <summary>
		/// Name of the connection string used by default. By default alphabetically first in appSettings.json/ConnectionStrings
		/// </summary>
		#endif
		public static string ConnectionStringName { get; set; } = ReadConnectionString();
		/// <summary>
		/// Error logger object
		/// </summary>
		public static OracleErrorLogger ErrorLogger { get; set; } = new OracleErrorLogger();
		/// <summary>
		/// Error logger method
		/// </summary>
		public static ErrorLoggerDelegate ErrorLoggerFunc { get; set; } = null;
		/// <summary>
		/// Is connection string raw connection string or name refernce to appsettings.json/ConnectionStrings 
		/// </summary>
		public static bool UseRawConnectionString { get; set; }
		/// <summary>
		/// Are dates forced to be local time regardless of DateTime.Kind value
		/// </summary>
		public static bool ForceLocalDate { get; set; } = true;
		/// <summary>
		/// Are parameters bound by their name (instead of position)
		/// </summary>
		public static bool BindParametersByName { get; set; } = true;
		/// <summary>
		/// How many times ORA-12516 error is ignored while trying to open the connection
		/// </summary>
		public static byte ConnectionTryCount { get; set; } = 100;
		/// <summary>
		/// Is OracleDbType.Byte datatype used for boolean values
		/// </summary>
		public static bool UseByteForBoolean { get; set; } = true;

		private static Dictionary<string, string> _connectionStrings;

		/// <summary>
		/// Return value for successfull procedure call when using NonQuery
		/// </summary>
		public const int ProcedureCallSuccess = -1;
		/// <summary>
		/// Return value for failed procedure call when using NonQuery
		/// </summary>
		public static int? NonQueryFail { get { return null; } }
		/// <summary>
		/// Status code for succesful execution
		/// </summary>
		public const int StatusCodeOk = 200;
		/// <summary>
		/// Status code for "ORA-01403 no data found" error
		/// </summary>
		public const int StatusCodeNotFound = 204;
		/// <summary>
		/// Status code for an error
		/// </summary>
		public const int StatusCodeError = 500;

		/// <summary>
		/// Last exception caused by a database call
		/// </summary>
		public Exception LastException { get; internal set; } = null;
		/// <summary>
		/// Last Oracle error code by a database call
		/// </summary>
		public string LastErrorCode { get; internal set; } = null;
		/// <summary>
		/// Last status code by a database call
		/// </summary>
		public int LastStatusCode { get; internal set; } = StatusCodeOk;

		private OracleConnection _connection = null;
		private OracleTransaction _transaction = null;
		#endregion

		#region Initialization
		private static string ReadConnectionString()
		{
		#if NETFRAMEWORK
		/*  Get the first connection string by default
		 *  - drops SqlServer connection strings
		 *  - pushes default ASP.NET Oracle connection string at the back of the list */
			_connectionStrings = ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>()
				.Where(x => x.ProviderName != "System.Data.SqlClient" && x.ConnectionString.Trim().Length > 0)
				.ToDictionary(k => k.Name, v => v.ConnectionString);
		#elif NETCORE
			IConfigurationBuilder builder = new ConfigurationBuilder();

			builder.AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json"), true);

			_connectionStrings = builder.Build()
				.GetSection("ConnectionStrings")
				.GetChildren()
					.ToDictionary(k => k.Key, v => v.Value);
		#endif
			if (_connectionStrings.Count == 0)
			{
				UseRawConnectionString = true;

				return null;
			}
			else
			{
				UseRawConnectionString = false;

				return _connectionStrings.First().Key;
			}
		}

		/// <summary>
		/// Opens new connection using default connection string and try count
		/// </summary>
		public OracleDatabase()
		{
			OpenConnection(ConnectionStringName, ConnectionTryCount);
		}
		/// <summary>
		/// Opens new connection using default try count
		/// </summary>
		/// <param name="connectionString">Connection string name or data (depending on UseRawConnectionString) using default try count</param>
		public OracleDatabase(string connectionString)
		{
			OpenConnection(connectionString, ConnectionTryCount);
		}
		/// <summary>
		/// Opens new connection using default connection string
		/// </summary>
		/// <param name="connectionTryCount">How many times ORA-12516 error is ignored while trying to open the connection</param>
		public OracleDatabase(byte connectionTryCount)
		{
			OpenConnection(ConnectionStringName, connectionTryCount);
		}
		/// <summary>
		/// Opens new connection
		/// </summary>
		/// <param name="connectionString">Connection string name or data (depending on UseRawConnectionString) using default try count</param>
		/// <param name="connectionTryCount">How many times ORA-12516 error is ignored while trying to open the connection</param>
		public OracleDatabase(string connectionString, byte connectionTryCount)
		{
			OpenConnection(connectionString, connectionTryCount);
		}

		private void OpenConnection(string connectionString, byte connectionTryCount)
		{
			int tryCount = 0;

			if (UseRawConnectionString)
			{
				_connection = new OracleConnection(connectionString);
			}
			else
			{
				_connection = new OracleConnection(_connectionStrings[connectionString]);
			}

			while (true)
			{
				try
				{
					_connection.Open();
					break;
				}
				catch (OracleException exception)
				{
					if (exception.Message.StartsWith("ORA-12516") && tryCount < connectionTryCount)
					{
						++tryCount;
						Thread.Sleep(1);
						continue;
					}
					LogOracleError(exception, new OracleCommand());
					throw exception;
				}
				catch (Exception exception)
				{
					LogOtherError(exception);
					throw exception;
				}
			}

			if (tryCount > 0)
			{
				ErrorLogger.LogError("ORA-12516", new Exception("Retried " + tryCount + " times"));
			}
		}

		/// <summary>
		/// Closes the connection, rolls back possible transaction and disposes the object
		/// </summary>
		public void Dispose()
		{
			if (_transaction != null)
			{
				_transaction.Rollback();
			}
			_connection.Close();
		}
		#endregion

		#region Public methods
		/// <summary>
		/// Starts new transaction
		/// </summary>
		public void BeginTransaction()
		{
			_transaction = _connection.BeginTransaction();
		} // BeginTransaction()

		/// <summary>
		/// Commits active transaction
		/// </summary>
		public void Commit()
		{
			_transaction.Commit();
			_transaction = null;
		} // Commit()

		/// <summary>
		/// Rolls back active transaction
		/// </summary>
		public void Rollback()
		{
			_transaction.Rollback();
			_transaction = null;
		} // Rollback()

		/// <summary>
		/// Creates a OracleCommand object for a procedure call
		/// </summary>
		/// <param name="name">Procedure name</param>
		public OracleCommand NewSPCommand(string name)
		{
			return new OracleCommand(name, _connection)
			{
				BindByName = BindParametersByName,
				CommandType = CommandType.StoredProcedure
			};
		} // NewSPCommand(string name)

		/// <summary>
		/// Executes a query which returns only one row (extra data will be ignored)
		/// </summary>
		/// <param name="commandText">SQL to be executed</param>
		public DataRow QueryRow(string commandText)
		{
			DataTable dataTable = QueryTable(commandText);

			if (dataTable == null)
			{
				return null;
			}
			if (dataTable.Rows.Count == 0)
			{
				LastStatusCode = StatusCodeNotFound;
				LastErrorCode = "ORA-01403";
				LastException = new Exception("ORA-01403: no data found");
				return null;
			}

			return dataTable.Rows[0];
		} // QueryRow(string commandText)

		/// <summary>
		/// Executes a query which returns one recordset (other recordsets will be ignored)
		/// </summary>
		/// <param name="commandText">SQL to be executed</param>
		public DataTable QueryTable(string commandText)
		{
			OracleCommand command = null;

			try
			{
				command = new OracleCommand(commandText, _connection)
				{
					BindByName = BindParametersByName
				};

				if (_transaction != null)
				{
					command.Transaction = _transaction;
				}

				OracleDataReader reader = command.ExecuteReader();
				DataTable dataTable = new DataTable();

				dataTable.Load(reader);

				LastException = null;
				LastErrorCode = null;
				LastStatusCode = StatusCodeOk;

				return dataTable;
			}
			catch (OracleException exception)
			{
				if (LogOracleError(exception, command))
				{
					return new DataTable();
				}
			}
			catch (Exception exception)
			{
				LogOtherError(exception);
			}

			return null;
		} // QueryTable(string commandText)

		/// <summary>
		/// Executes a query which returns only one row (extra data will be ignored)
		/// </summary>
		/// <param name="command">Command to be executed</param>
		public DataRow QueryRow(OracleCommand command)
		{
			DataTable dataTable = QueryTable(command);

			if (dataTable == null)
			{
				return null;
			}
			if (dataTable.Rows.Count == 0)
			{
				LastStatusCode = StatusCodeNotFound;
				LastErrorCode = "ORA-01403";
				LastException = new Exception("ORA-01403: no data found");
				return null;
			}

			return dataTable.Rows[0];
		} // QueryRow(OracleCommand command)

		/// <summary>
		/// Executes a query which returns one recordset (other recordsets will be ignored)
		/// </summary>
		/// <param name="command">Command to be executed</param>
		public DataTable QueryTable(OracleCommand command)
		{
			DataSet dataSet = QuerySet(command);

			if (dataSet == null)
			{
				return null;
			}
			if (dataSet.Tables.Count == 0)
			{
				return new DataTable();
			}

			return dataSet.Tables[0];
		} // QueryTable(OracleCommand command)

		/// <summary>
		/// Executes a query which returns several recordsets
		/// </summary>
		/// <param name="command">Command to be executed</param>
		public DataSet QuerySet(OracleCommand command)
		{
			try
			{
				command.Connection = _connection;
				command.BindByName = BindParametersByName;
				if (_transaction != null)
				{
					command.Transaction = _transaction;
				}

				DataSet dataSet = new DataSet();
				DataTable dataTable = new DataTable();

				CheckCommand(command);

				if (command.CommandType == CommandType.StoredProcedure)
				{
					List<DataTable> cursorTables = new List<DataTable>();

					command.ExecuteNonQuery();

					DataRow dataRow = dataTable.NewRow();
					bool hasData = false;

					foreach (OracleParameter parameter in command.Parameters)
					{
						if (parameter.Direction == ParameterDirection.Input)
						{
							continue;
						}

						DataColumn column = null;
						object data = null;
						string name = StripPrefix(parameter.ParameterName);

						switch (parameter.OracleDbType)
						{
							case OracleDbType.Boolean:
							{
								column = new DataColumn(name, typeof(bool));
								data = (bool?)(OracleBoolean)ColumnDataFromDecimal(parameter.Value) ?? (object)DBNull.Value;
								break;
							}
							case OracleDbType.Byte:
							{
								column = new DataColumn(name, typeof(byte));
								data = (byte?)(OracleDecimal)ColumnDataFromDecimal(parameter.Value) ?? (object)DBNull.Value;
								break;
							}
							case OracleDbType.Int16:
							{
								column = new DataColumn(name, typeof(short));
								data = (short?)(OracleDecimal)ColumnDataFromDecimal(parameter.Value) ?? (object)DBNull.Value;
								break;
							}
							case OracleDbType.Int32:
							{
								column = new DataColumn(name, typeof(int));
								data = (int?)(OracleDecimal)ColumnDataFromDecimal(parameter.Value) ?? (object)DBNull.Value;
								break;
							}
							case OracleDbType.Int64:
							{
								column = new DataColumn(name, typeof(long));
								data = (long?)(OracleDecimal)ColumnDataFromDecimal(parameter.Value) ?? (object)DBNull.Value;
								break;
							}
							case OracleDbType.Single:
							{
								column = new DataColumn(name, typeof(float));
								data = (float?)(OracleDecimal)ColumnDataFromDecimal(parameter.Value) ?? (object)DBNull.Value;
								break;
							}
							case OracleDbType.Double:
							{
								column = new DataColumn(name, typeof(double));
								data = (double?)(OracleDecimal)ColumnDataFromDecimal(parameter.Value) ?? (object)DBNull.Value;
								break;
							}
							case OracleDbType.Decimal:
							{
								column = new DataColumn(name, typeof(decimal));
								data = (decimal?)(OracleDecimal)ColumnDataFromDecimal(parameter.Value) ?? (object)DBNull.Value;
								break;
							}
							case OracleDbType.Date:
							case OracleDbType.TimeStamp:
							{
								column = new DataColumn(name, typeof(DateTime));
								if (((OracleDate)parameter.Value).IsNull)
								{
									data = DBNull.Value;
								}
								else
								{
									data = (DateTime?)(OracleDate)parameter.Value;
								}
								break;
							}
							case OracleDbType.NVarchar2:
							case OracleDbType.Varchar2:
							{
								column = new DataColumn(name, typeof(string));
								if (((OracleString)parameter.Value).IsNull)
								{
									data = DBNull.Value;
								}
								else
								{
									data = (string)(OracleString)parameter.Value;
								}
								break;
							}
							case OracleDbType.Blob:
							{
								column = new DataColumn(name, typeof(byte[]));
								OracleBlob blob = (OracleBlob)parameter.Value;

								if (blob.IsNull)
								{
									data = DBNull.Value;
								}
								else
								{
									data = blob.Value;
								}
								break;
							}
							case OracleDbType.Clob:
							case OracleDbType.NClob:
							{
								column = new DataColumn(name, typeof(string));
								OracleClob clob = (OracleClob)parameter.Value;

								if (clob.IsNull)
								{
									data = DBNull.Value;
								}
								else
								{
									data = clob.Value;
								}
								break;
							}
							case OracleDbType.RefCursor:
							{
								DataTable cursorTable = new DataTable
								{
									TableName = name
								};
								cursorTable.Load(((OracleRefCursor)parameter.Value).GetDataReader());
								cursorTables.Add(cursorTable);
								continue;
							}
							default:
							{
								throw new Exception("DataType not implemented");
							}
						} // switch (parameter.OracleDbType)

						if (data != null)
						{
							dataTable.Columns.Add(column);
							dataRow[name] = data;
							hasData = true;
						}
					} // foreach (command.Parameters)

					if (hasData)
					{
						dataTable.Rows.Add(dataRow);
						dataSet.Tables.Add(dataTable);
					}

					if (cursorTables.Count > 0)
					{
						foreach (DataTable cursorTable in cursorTables)
						{
							dataSet.Tables.Add(cursorTable);
						}
					}
				}
				else // if (command.CommandType == CommandType.StoredProcedure)
				{
					OracleDataReader reader = command.ExecuteReader();

					dataTable.TableName = "_OUTPUT";
					dataTable.Load(reader);
					dataSet.Tables.Add(dataTable);
				} // else (command.CommandType == CommandType.StoredProcedure)

				LastException = null;
				LastStatusCode = StatusCodeOk;

				return dataSet;
			}
			catch (OracleException exception)
			{
				if (LogOracleError(exception, command))
				{
					return null;
				}
			}
			catch (Exception exception)
			{
				LogOtherError(exception);
			}

			return null;
		} // QuerySet(OracleCommand command)

		/// <summary>
		/// Executes a query which does not return recordsets
		/// </summary>
		/// <param name="commandText">SQL to be executed</param>
		public int? NonQuery(string commandText)
		{
			OracleCommand command = null;

			try
			{
				command = new OracleCommand(commandText, _connection)
				{
					BindByName = BindParametersByName
				};

				if (_transaction != null)
				{
					command.Transaction = _transaction;
				}

				LastException = null;
				LastStatusCode = StatusCodeOk;

				CheckCommand(command);

				return command.ExecuteNonQuery();
			}
			catch (OracleException exception)
			{
				LogOracleError(exception, command);
			}
			catch (Exception exception)
			{
				LogOtherError(exception);
			}

			return NonQueryFail;
		} // NonQuery(string commandText)

		/// <summary>
		/// Executes a query which does not return recordsets
		/// </summary>
		/// <param name="command">Command to be executed</param>
		public int? NonQuery(OracleCommand command)
		{
			try
			{
				command.Connection = _connection;
				command.BindByName = BindParametersByName;

				if (_transaction != null)
				{
					command.Transaction = _transaction;
				}

				LastException = null;
				LastStatusCode = StatusCodeOk;

				CheckCommand(command);

				return command.ExecuteNonQuery();
			}
			catch (OracleException exception)
			{
				LogOracleError(exception, command);
			}
			catch (Exception exception)
			{
				LogOtherError(exception);
			}

			return null;
		} // NonQuery(OracleCommand command)
		#endregion

		#region Static methods
		/// <summary>
		/// Inserts data from a DataTable into an OracleCommand for bulk insert
		/// </summary>
		/// <param name="command">Target object</param>
		/// <param name="data">Data which to insert</param>
		/// <param name="columns">List of column names to be set</param>
		public static void BulkSet(OracleCommand command, DataTable data, string[] columns = null)
		{
			Dictionary<OracleParameter, string> parameterNames = new Dictionary<OracleParameter, string>();

			if (columns == null)
			{
				foreach (DataColumn column in data.Columns)
				{
					OracleParameter parameter = command.Parameters[InputPrefix + column.ColumnName];

					if (parameter == null && InputOutputPrefix.Length > 0)
					{
						parameter = command.Parameters[InputOutputPrefix + column.ColumnName];
					}
					if (parameter == null)
					{
						parameter = new OracleParameter(InputPrefix + column.ColumnName, ToOracleDbType(column.DataType), ParameterDirection.Input);
						command.Parameters.Add(parameter);
					}

					parameterNames.Add(parameter, column.ColumnName);
				}
			}
			else // (columns != null)
			{
				foreach (string columnName in columns)
				{
					DataColumn column = data.Columns[columnName.ToUpper()];

					if (data.Columns[columnName] == null)
					{
						throw new Exception("Column not found: " + columnName);
					}

					OracleParameter parameter = command.Parameters[InputPrefix + column.ColumnName];

					if (parameter == null && InputOutputPrefix.Length > 0)
					{
						parameter = command.Parameters[InputOutputPrefix + column.ColumnName];
					}
					if (parameter == null)
					{
						parameter = new OracleParameter(InputPrefix + column.ColumnName, ToOracleDbType(column.DataType), ParameterDirection.Input);
						command.Parameters.Add(parameter);
					}

					parameterNames.Add(parameter, column.ColumnName);
				}
			} // else (columns != null)

			command.ArrayBindCount = data.Rows.Count;

			foreach (KeyValuePair<OracleParameter, string> item in parameterNames)
			{
				BulkSet(item.Key, data, item.Value);
			}
		} // BulkSet(OracleCommand command, DataTable data, bool insertMissing = false, List<string> columns = null)

		/// <summary>
		/// Inserts data from a DataTable into an OracleCommand for bulk insert
		/// </summary>
		/// <param name="command">Target object</param>
		/// <param name="data">Data which to insert</param>
		/// <param name="columns">Name of a column to be set</param>
		public static void BulkSet(OracleParameter parameter, DataTable data, string columnName)
		{
			switch (parameter.OracleDbType)
			{
				case OracleDbType.Boolean:
				{
					parameter.Value = ToArray<bool>(data, columnName);
					break;
				}
				case OracleDbType.Byte:
				{
					parameter.Value = ToArray<byte>(data, columnName);
					break;
				}
				case OracleDbType.Int16:
				{
					parameter.Value = ToArray<short>(data, columnName);
					break;
				}
				case OracleDbType.Int32:
				{
					parameter.Value = ToArray<int>(data, columnName);
					break;
				}
				case OracleDbType.Int64:
				{
					parameter.Value = ToArray<long>(data, columnName);
					break;
				}
				case OracleDbType.Single:
				{
					parameter.Value = ToArray<float>(data, columnName);
					break;
				}
				case OracleDbType.Double:
				{
					parameter.Value = ToArray<double>(data, columnName);
					break;
				}
				case OracleDbType.Decimal:
				{
					parameter.Value = ToArray<decimal>(data, columnName);
					break;
				}
				case OracleDbType.Date:
				case OracleDbType.TimeStamp:
				{
					parameter.Value = ToArray<DateTime>(data, columnName);
					break;
				}
				case OracleDbType.NVarchar2:
				case OracleDbType.Varchar2:
				case OracleDbType.Clob:
				case OracleDbType.NClob:
				{
					parameter.Value = ToArray<string>(data, columnName);
					break;
				}
				case OracleDbType.Blob:
				{
					parameter.Value = ToArray<byte[]>(data, columnName);
					break;
				}
				default:
				{
					throw new Exception("OracleDbType not implemented");
				}
			} // switch (parameter.OracleDbType)
		} // BulkSet(OracleParameter parameter, DataTable data, string columnName)

		/// <summary>
		/// Inserts data from a list of classes into an OracleCommand for bulk insert
		/// </summary>
		/// <param name="command">Target object</param>
		/// <param name="data">Data which to insert</param>
		/// <param name="columns">List of column names to be set</param>
		public static void BulkSet<T>(OracleCommand command, List<T> data, string[] columns = null) where T : class, new()
		{
			Dictionary<OracleParameter, string> parameterNames = new Dictionary<OracleParameter, string>();
			Dictionary<string, Type> columnTypes = MapClass<T>(new T(), ParameterDirection.Output);

			command.ArrayBindCount = data.Count;

			if (columns == null)
			{
				foreach (var column in columnTypes)
				{
					OracleParameter parameter = command.Parameters[InputPrefix + column.Key];

					if (parameter == null && InputOutputPrefix.Length > 0)
					{
						parameter = command.Parameters[InputOutputPrefix + column.Key];
					}
					if (parameter == null)
					{
						parameter = new OracleParameter(InputPrefix + column.Key, ToOracleDbType(column.Value), ParameterDirection.Input);
						command.Parameters.Add(parameter);
					}

					parameterNames.Add(parameter, column.Key);
				}
			}
			else // (columns != null)
			{
				foreach (string columnName in columns)
				{
					if (!columnTypes.ContainsKey(columnName))
					{
						throw new Exception("Column not found " + columnName);
					}

					OracleParameter parameter = command.Parameters[InputPrefix + columnName];

					if (parameter == null && InputOutputPrefix.Length > 0)
					{
						parameter = command.Parameters[InputOutputPrefix + columnName];
					}
					if (parameter == null)
					{
						parameter = new OracleParameter(InputPrefix + columnName, ToOracleDbType(columnTypes[columnName]), ParameterDirection.Input);
						command.Parameters.Add(parameter);
					}

					parameterNames.Add(parameter, columnName);
				}
			} // (columns != null)

			foreach (KeyValuePair<OracleParameter, string> item in parameterNames)
			{
				item.Key.Value = data
					.Select(i => ReadValue(i, ToClassName(item.Value)) ?? DBNull.Value)
					.ToArray();
			}
		} // BulkSet<T>(OracleCommand command, List<T> data, string[] columns = null)

		/// <summary>
		/// Inserts data from a list into an OracleCommand for bulk insert
		/// </summary>
		/// <param name="command">Target object</param>
		/// <param name="data">Data which to insert</param>
		/// <param name="columnName">name of the column to be set</param>
		public static void BulkSet<T>(OracleCommand command, List<T> data, string columnName)
		{
			BulkSet(command, data.ToArray(), columnName);
		} // BulkSet<T>(OracleCommand command, List<T> data, string columnName)

		/// <summary>
		/// Inserts data from an IEnumerable into an OracleCommand for bulk insert
		/// </summary>
		/// <param name="command">Target object</param>
		/// <param name="data">Data which to insert</param>
		/// <param name="columnName">name of the column to be set</param>
		public static void BulkSet<T>(OracleCommand command, IEnumerable<T> data, string columnName)
		{
			BulkSet(command, data.ToArray(), columnName);
		} // BulkSet<T>(OracleCommand command, IEnumerable<T> data, string columnName)

		/// <summary>
		/// Inserts data from an array into an OracleCommand for bulk insert
		/// </summary>
		/// <param name="command">Target object</param>
		/// <param name="data">Data which to insert</param>
		/// <param name="columnName">name of the column to be set</param>
		public static void BulkSet<T>(OracleCommand command, T[] data, string columnName)
		{
			OracleParameter parameter = command.Parameters[InputPrefix + columnName];

			if (parameter == null && InputOutputPrefix.Length > 0)
			{
				parameter = command.Parameters[InputOutputPrefix + columnName];
			}
			if (parameter == null)
			{
				parameter = new OracleParameter(InputPrefix + columnName, ToOracleDbType(typeof(T)), ParameterDirection.Input);
				command.Parameters.Add(parameter);
			}

			parameter.Value = data;

			command.ArrayBindCount = data.Count();
		} // BulkSet<T>(OracleCommand command, T[] data, string columnName)

		/// <summary>
		/// Fills a parameter in OracleCommand with a value for bulk insert using ArrayBindCount
		/// </summary>
		/// <param name="command">Target object</param>
		/// <param name="parameterName">Parameter to fill</param>
		/// <param name="parameterValue">Value to fill with</param>
		public static OracleParameter BulkFill(OracleCommand command, string parameterName, object parameterValue)
		{
			return BulkFill(command, parameterName, null, null, parameterValue);
		}
		/// <summary>
		/// Fills a parameter in OracleCommand with a value for bulk insert using ArrayBindCount
		/// </summary>
		/// <param name="command">Target object</param>
		/// <param name="parameterName">Parameter to fill</param>
		/// <param name="parameterType">Parameter type</param>
		/// <param name="parameterValue">Value to fill with</param>
		public static OracleParameter BulkFill(OracleCommand command, string parameterName, OracleDbType? parameterType, object parameterValue)
		{
			return BulkFill(command, parameterName, parameterType, null, parameterValue);
		}
		/// <summary>
		/// Fills a parameter in OracleCommand with a value for bulk insert using ArrayBindCount
		/// </summary>
		/// <param name="command">Target object</param>
		/// <param name="parameterName">Parameter to fill</param>
		/// <param name="parameterType">Parameter type</param>
		/// <param name="parameterSize">Parameter size</param>
		/// <param name="parameterValue">Value to fill with</param>
		public static OracleParameter BulkFill(OracleCommand command, string parameterName, OracleDbType? parameterType, int? parameterSize, object parameterValue)
		{
			OracleParameter parameter;

			if (command.Parameters.Contains(parameterName))
			{
				parameter = command.Parameters[parameterName];
			}
			else if (parameterType.HasValue)
			{
				if (parameterSize.HasValue)
				{
					parameter = command.Parameters.Add(parameterName, parameterType.Value, parameterSize.Value, ParameterDirection.Input);
				}
				else
				{
					parameter = command.Parameters.Add(parameterName, parameterType.Value, ParameterDirection.Input);
				}
			}
			else
			{
				parameter = command.Parameters.Add(parameterName, ToOracleDbType(parameterValue.GetType()), ParameterDirection.Input);
			}

			parameter.Value = Enumerable.Repeat(parameterValue, command.ArrayBindCount).ToArray();

			return parameter;
		} // BulkFill(OracleCommand command, string parameterName, OracleDbType? parameterType, int? parameterSize, object parameterValue)

		/// <summary>
		/// Get OracleDbType from C# data type
		/// </summary>
		/// <param name="type">Data type</param>
		public static OracleDbType ToOracleDbType(Type type)
		{
			if (type == typeof(bool) || type == typeof(bool?))
			{
				if (UseByteForBoolean)
				{
					return OracleDbType.Byte;
				}
				else
				{
					return OracleDbType.Boolean;
				}
			}
			if (type == typeof(byte) || type == typeof(byte?))
			{
				return OracleDbType.Byte;
			}
			if (type == typeof(short) || type == typeof(short?))
			{
				return OracleDbType.Int16;
			}
			if (type == typeof(int) || type == typeof(int?))
			{
				return OracleDbType.Int32;
			}
			if (type == typeof(long) || type == typeof(long?))
			{
				return OracleDbType.Int64;
			}
			if (type == typeof(float) || type == typeof(float?))
			{
				return OracleDbType.Single;
			}
			if (type == typeof(double) || type == typeof(double?))
			{
				return OracleDbType.Double;
			}
			if (type == typeof(decimal) || type == typeof(decimal?))
			{
				return OracleDbType.Decimal;
			}
			if (type == typeof(DateTime) || type == typeof(DateTime?))
			{
				return OracleDbType.Date;
			}
			if (type == typeof(string))
			{
				return OracleDbType.Varchar2;
			}
			if (type == typeof(byte[]))
			{
				return OracleDbType.Blob;
			}
			throw new Exception("Unsupported data type");
		} // ToOracleDbType(Type type)
		#endregion

		#region Private methods
		private static void CheckCommand(OracleCommand command)
		{
			if (ForceLocalDate || UseByteForBoolean)
			{
				foreach (OracleParameter parameter in command.Parameters)
				{
					if (UseByteForBoolean && parameter.OracleDbType == OracleDbType.Boolean)
					{
						parameter.OracleDbType = OracleDbType.Byte;
					}
					else if (
						ForceLocalDate
						&& (parameter.Value != null && parameter.Value != DBNull.Value)
						&& (parameter.OracleDbType == OracleDbType.Date || parameter.OracleDbType == OracleDbType.TimeStamp)
					)
					{
						if (command.ArrayBindCount > 0)
						{
							object[] parameterValues = (object[])parameter.Value;

							for (int i = 0; i < parameterValues.Length; i++)
							{
								if (parameterValues[i] != DBNull.Value && parameterValues[i] != null)
								{
									parameterValues[i] = ForceLocalDateKind((DateTime)parameterValues[i]);
								}
							}

							parameter.Value = parameterValues;
						}
						else
						{
							parameter.Value = ForceLocalDateKind((DateTime)parameter.Value);
						}
					}
				} // foreach (OracleParameter parameter in command.Parameters)
			} // if (ForceLocalDate || UseByteForBoolean)
		} // CheckCommand()

		private static DateTime ForceLocalDateKind(DateTime date)
		{
			if (date.Kind == DateTimeKind.Unspecified)
			{
				DateTime.SpecifyKind(date, DateTimeKind.Local);
			}
			else
			{
				date = date.ToLocalTime();
			}

			return date;
		} // ForceLocalDateKind()

		private static object ColumnDataFromDecimal(object value)
		{
			if (((OracleDecimal)value).IsNull)
			{
				return null;
			}
			return (OracleDecimal)value;
		} // ColumnDataFromDecimal(object value)

		private static object ReadValue(object input, string name)
		{
			Type type = input.GetType();
			TypedReference refInput = __makeref(input);

			foreach (FieldInfo field in type.GetFields())
			{
				if (field.Name == name)
				{
					return field.GetValueDirect(refInput);
				}
			}

			foreach (PropertyInfo property in type.GetProperties())
			{
				if (property.Name == name)
				{
					if (!property.CanRead)
					{
						return null;
					}
					return property.GetValue(input);
				}
			}

			return null;
		} // ReadValue(object input, string name)

		private static object ReadValue<T>(object input)
		{
			Type type = typeof(T);

			if (ReadValue(type, input, out object output))
			{
				if (output == null)
				{
					return default(T);
				}
				return output;
			}
			return default(T);
		} // ReadValue<T>(object input)

		private static bool ReadValue(Type type, object input, out object output)
		{
			if (input == DBNull.Value)
			{
				output = null;
				return true;
			}
			if (type == typeof(bool) || type == typeof(bool?))
			{
				Type typeIn = input.GetType();

				if (typeIn == typeof(decimal))
				{
					output = ((decimal)input != 0);
				}
				else
				{
					output = (Convert.ToInt32(input) != 0);
				}
				return true;
			}
			if (type == typeof(float) || type == typeof(float?))
			{
				if (input.GetType() == typeof(double))
				{
					output = (float)(double)input;
				}
				else
				{
					output = Convert.ToSingle(input);
				}
				return true;
			}
			if (type == typeof(double) || type == typeof(double?))
			{
				if (input.GetType() == typeof(double))
				{
					output = (double)input;
				}
				else
				{
					output = Convert.ToDouble(input);
				}
				return true;
			}
			if (type == typeof(decimal) || type == typeof(decimal?))
			{
				if (input.GetType() == typeof(double))
				{
					output = (decimal)(double)input;
				}
				else
				{
					output = Convert.ToDecimal(input);
				}
				return true;
			}
			if (type == typeof(short) || type == typeof(short?))
			{
				if (input.GetType() == typeof(decimal))
				{
					output = (short)(decimal)input;
				}
				else
				{
					output = Convert.ToInt16(input);
				}
				return true;
			}
			if (type == typeof(int) || type == typeof(int?))
			{
				if (input.GetType() == typeof(decimal))
				{
					output = (int)(decimal)input;
				}
				else
				{
					output = Convert.ToInt32(input);
				}
				return true;
			}
			if (type == typeof(long) || type == typeof(long?))
			{
				if (input.GetType() == typeof(decimal))
				{
					output = (long)(decimal)input;
				}
				else
				{
					output = Convert.ToInt64(input);
				}
				return true;
			}
			if (type == typeof(string))
			{
				output = (string)input;
				return true;
			}
			if (type == typeof(DateTime) || type == typeof(DateTime?))
			{
				output = (DateTime?)input;
				return true;
			}

			output = null;
			return false;
		} // ReadValue(Type type, object input, out object output)

		private bool LogOracleError(OracleException exception, OracleCommand command)
		{
			LastException = exception;
			LastErrorCode = exception.Message.Substring(0, 9);

			switch (LastErrorCode)
			{
				case "ORA-01403":
					LastStatusCode = StatusCodeNotFound;
					return true;
				default:
					LastStatusCode = StatusCodeError;
					break;
			}

			if (ErrorLogger != null || ErrorLogger != null)
			{
				Dictionary<string, string> parameters = new Dictionary<string, string>();

				foreach (OracleParameter parameter in command.Parameters)
				{
					parameters.Add(parameter.ParameterName, parameter.Value.ToString());
				}

				if (ErrorLogger != null)
				{
					ErrorLogger.LogError(LastErrorCode, exception, command.CommandText, parameters);
				}

				ErrorLoggerFunc?.Invoke(exception, command.CommandText, parameters);
			}

			return false;
		} // LogOracleError(OracleException oe, OracleCommand command)

		private void LogOtherError(Exception exception)
		{
			LastException = exception;
			LastErrorCode = null;
			LastStatusCode = StatusCodeError;

			if (ErrorLogger != null)
			{
				ErrorLogger.LogError(".NET ERROR", exception);
			}
			ErrorLoggerFunc?.Invoke(exception);
		} // LogOtherError(Exception e)
#endregion
	}

	/// <summary>
	/// Default error object
	/// </summary>
	public class OracleErrorLogger
	{
		/// <summary>
		/// Called method
		/// </summary>
		public virtual void LogError(string errorCode, Exception exception, string commandText = null, Dictionary<string, string> parameters = null)
		{
			Debug.WriteLine("{0:s} {1} {2} {3}", DateTime.Now, errorCode, exception.Message, exception.Source);
			Trace.TraceError("{0:s} {1} {2} {3}", DateTime.Now, errorCode, exception.Message, exception.Source);
		}
	}
}